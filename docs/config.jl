using QuadLearn

package_info = Dict(
    "modules" => [QuadLearn],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "QuadLearn.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/QuadLearn",
    "pages" => [
        "About"  =>  "index.md"
        "Tutorial" => "tutorial.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(QuadLearn, :DocTestSetup, :(using QuadLearn); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(QuadLearn, fix=true)
end

# disable display support for `Plots`
# (important for plotting on headless machines, i.e. gitlab runner)
ENV["GKSwstype"] = "nul"
