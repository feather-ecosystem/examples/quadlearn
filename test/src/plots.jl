
using Plots

Θ = LinRange(0.0,2*pi, 120)
plot(QuadLearn.x_coord_on_circle(r,Θ), QuadLearn.y_coord_on_circle(r,Θ), color=:red, linewidth=2, legend = false, aspect_ratio=:equal)

X = collect(element)
plot!(X[1], X[2], color=:black, linewidth=2)
plot!(X[1]', X[2]', color=:black, linewidth=2)

e_ref = QuadLearn.rotate_to_reference_frame(element)
X = collect(e_ref)
plot!(X[1], X[2], color=:black, linewidth=2)
plot!(X[1]', X[2]', color=:black, linewidth=2)

if QuadLearn.is_point_on_south_boundary(r,e_ref)
    point = QuadLearn.get_point_on_south_boundary(r,e_ref)
    scatter!(point, color=:blue)
end

if QuadLearn.is_point_on_north_boundary(r,e_ref)
    point = QuadLearn.get_point_on_north_boundary(r,e_ref)
    scatter!(point, color=:blue)
end

if QuadLearn.is_point_on_east_boundary(r,e_ref)
    point = QuadLearn.get_point_on_east_boundary(r,e_ref)
    scatter!(point, color=:blue)
end

if QuadLearn.is_point_on_west_boundary(r,e_ref)
    point = QuadLearn.get_point_on_west_boundary(r,e_ref)
    scatter!(point, color=:blue)
end