export compute_area_cut_element

# center of Interval and element
center(I::Interval) = 0.5 * (I.a+I.b)
center(I...) = map(center,I...)

# helper functions
x_coord_on_circle(r,θ) = r .* cos.(θ)
y_coord_on_circle(r,θ) = r .* sin.(θ) 

Zx(r, xbar) = r * sin(acos(xbar/r))
Zy(r, ybar) = r * cos(asin(ybar/r))

# measure intervals and elements
measure(I::Interval) = I.b-I.a
measure(element) = measure(element.data[1]) * measure(element.data[2])
area(element) = measure(element)

# mean of two things
mean(x,y) = 0.5 .* (x .+ y)

# rotate
function rotate_to_reference_frame(p)
    return (abs(p[1]), abs(p[2]))
end

# rotate an element into the referene frame
function rotate_to_reference_frame(element::CartesianProduct)
    c = rotate_to_reference_frame(center(element))
    sx, sy = measure(element.data[1]), measure(element.data[2])
    return Interval(c[1]-0.5*sx, c[1]+0.5*sx) ⨱ Interval(c[2]-0.5*sy, c[2]+0.5*sy)
end

# check if input point lies on the north boundary
function check_point_is_on_north_boundary(element, p)
    X, Y = element.data
    if p[2] == Y.b && X.a <= p[1] <= X.b
        return true
    end
    return false
end

# check if input point lies on the south boundary
function check_point_is_on_south_boundary(element, p)
    X, Y = element.data
    if p[2] == Y.a && X.a <= p[1] <= X.b
        return true
    end
    return false
end

# check if input point lies on the east boundary
function check_point_is_on_east_boundary(element, p)
    X, Y = element.data
    if p[1] == X.b && Y.a <= p[2] <= Y.b
        return true
    end
    return false
end

# check if input point lies on the west boundary
function check_point_is_on_west_boundary(element, p)
    X, Y = element.data
    if p[1] == X.a && Y.a <= p[2] <= Y.b
        return true
    end
    return false
end

# check if there is a point on the south boundary
function is_point_on_south_boundary(R, element)
    X, Y = element.data
    if abs(Y.a/R)<=1
        xbar = Zy(R,Y.a)
        if X.a <= xbar <= X.b || X.a <= -xbar <= X.b
            return true
        end
    end
    return false
end

# check if there is a point on the north boundary
function is_point_on_north_boundary(R, element)
    X, Y = element.data
    if abs(Y.b/R)<=1
        xbar = Zy(R,Y.b)
        if X.a <= xbar <= X.b || X.a <= -xbar <= X.b
            return true
        end
    end
    return false
end

# check if there is a point on the east boundary
function is_point_on_east_boundary(R, element)
    X, Y = element.data
    if abs(X.b/R)<=1
        ybar = Zy(R,X.b)
        if Y.a <= ybar <= Y.b || Y.a <= -ybar <= Y.b
            return true
        end
    end
    return false
end

# check if there is a point on the west boundary
function is_point_on_west_boundary(R, element)
    X, Y = element.data
    if abs(X.a/R)<=1
        ybar = Zy(R,X.a)
        if Y.a <= ybar <= Y.b || Y.a <= -ybar <= Y.b
            return true
        end
    end
    return false
end

# get point on the south boundary
function get_point_on_south_boundary(R, element)
    X, Y = element.data
    xbar = Zy(R,Y.a)
    if X.a <= xbar <= X.b
        return (xbar,Y.a)
    end
    if X.a <= -xbar <= X.b
        return (-xbar,Y.a)
    end
end

# get point on the north boundary
function get_point_on_north_boundary(R, element)
    X, Y = element.data
    xbar = Zy(R,Y.b)
    if X.a <= xbar <= X.b
        return (xbar,Y.b)
    end
    if X.a <= -xbar <= X.b
        return (-xbar,Y.b)
    end
end

# get point on the east boundary
function get_point_on_east_boundary(R, element)
    X, Y = element.data
    ybar = Zy(R,X.b)
    if Y.a <= ybar <= Y.b
        return (X.b,ybar)
    end
    if Y.a <= -ybar <= Y.b
        return (X.b,-ybar)
    end
end

# get point on the west boundary
function get_point_on_west_boundary(R, element)
    X, Y = element.data
    ybar = Zy(R,X.a)
    if Y.a <= ybar <= Y.b
        return (X.a,ybar)
    end
    if Y.a <= -ybar <= Y.b
        return (X.a,-ybar)
    end
end

# case 1
function approximate_area_cut_element_case_1(element, p1, p2)

    c1 = check_point_is_on_east_boundary(element,p1) && check_point_is_on_north_boundary(element,p2)
    @assert c1 "Points should lie on east and north boundary."

    Δt =  0.5 * abs((p2[1] - p1[1]) * (p2[2] - p1[2])) 
    return area(element) - Δt
end

# case 2
function approximate_area_cut_element_case_2(element, p1, p2)

    c = check_point_is_on_south_boundary(element,p1) && check_point_is_on_north_boundary(element,p2)
    @assert c "Points should lie on south and north boundary"

    X, Y = element.data
    Δx = mean(p1[1],p2[1]) - X.a   
    Δy = Y.b-Y.a
    return Δx * Δy 
end

# case 3
function approximate_area_cut_element_case_3(element, p1, p2)

    c = check_point_is_on_east_boundary(element,p1) && check_point_is_on_west_boundary(element,p2)
    @assert c "Points should lie on east and west boundary"

    X, Y = element.data
    Δx = X.b-X.a
    Δy = mean(p1[2],p2[2]) - Y.a   
    return Δx * Δy 
end

# case 4
function approximate_area_cut_element_case_4(element, p1, p2)

    c1 = check_point_is_on_west_boundary(element,p1) && check_point_is_on_south_boundary(element,p2)
    @assert c1 "Points should lie on west and south boundary."

    Δt =  0.5 * abs((p2[1] - p1[1]) * (p2[2] - p1[2])) 
    return Δt
end

# compute angle between two vectors
angle(p) = atan(p[2], p[1])

# compute area under an arc
function compute_area_under_arc(R,p1,p2)
    alpha = abs(angle(p2) - angle(p1))
    ΔA = 0.5 * alpha * R^2
    ΔT = R * cos(alpha/2) * R * sin(alpha/2)
    return ΔA - ΔT  
end

# approximate the area under an element cut by a circular disk
function compute_area_cut_element(element, R; exact=false)

    # initialize
    e_ref = rotate_to_reference_frame(element)

    ########
    # case 1
    ########
    if is_point_on_east_boundary(R, e_ref) && is_point_on_north_boundary(R, e_ref)
        p1, p2 = get_point_on_east_boundary(R, e_ref), get_point_on_north_boundary(R, e_ref)
        ΔA = approximate_area_cut_element_case_1(e_ref, p1, p2)
    end

    ########
    # case 2
    ########
    if is_point_on_south_boundary(R, e_ref) && is_point_on_north_boundary(R, e_ref)
        p1, p2 = get_point_on_south_boundary(R, e_ref), get_point_on_north_boundary(R, e_ref)
        ΔA = approximate_area_cut_element_case_2(e_ref, p1, p2)
    end

    ########
    # case 3
    ########
    if is_point_on_east_boundary(R, e_ref) && is_point_on_west_boundary(R, e_ref)
        p1, p2 = get_point_on_east_boundary(R, e_ref), get_point_on_west_boundary(R, e_ref)
        ΔA = approximate_area_cut_element_case_3(e_ref, p1, p2)
    end

    ########
    # case 4
    ########
    if is_point_on_west_boundary(R, e_ref) && is_point_on_south_boundary(R, e_ref)
        p1, p2 = get_point_on_west_boundary(R, e_ref), get_point_on_south_boundary(R, e_ref)
        ΔA = approximate_area_cut_element_case_4(e_ref, p1, p2)
    end

    # add part due to curved boundary
    if exact==true
        ΔA += compute_area_under_arc(R,p1,p2)
    end

    return ΔA
end