path_to_root = joinpath(dirname(pathof(Algoim)),"../")

# external c++ header paths
addHeaderDir(joinpath(path_to_root, "external/blitz-install/include"), kind=C_System)

# c++ dll's to include in project
Libdl.dlopen(joinpath(path_to_root, "external/blitz-install/lib", "libblitz.so"), Libdl.RTLD_GLOBAL)