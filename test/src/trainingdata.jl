export compute_moments_bernstein_basis, integrate_bernstein_basis, get_cut_domain, area_cut_element
export generate_training_data, get_bernsteinfuns_at_gausspoints

function integrate_bernstein_basis(q, map, element; n = 32)

    # domain of cut element
    Ix, Iy = element.data

    # compute bernstein polynomials
    bx = x -> bernstein(Ix, q, x)
    by = y -> bernstein(Iy, q, y)

    # allocate array for moments
    A = zeros(q+1,q+1)

    # get quadrature rule
    x, y = LinRange(Ix.a, Ix.b, n+1), LinRange(Iy.a, Iy.b, n+1)
    
    # loop over integration mesh
    for j in 1:n
        for i in 1:n
            Q = Algoim.eval(AlgoimQuadRule(map, Interval(x[i], x[i+1]), Interval(y[j], y[j+1]); order=6))
            integrate_bernstein_basis!(A, Q, bx, by)
        end
    end
    
    return A
end

function integrate_bernstein_basis!(A::Matrix, Q::AlgoimQuadRule, Bx::Function, By::Function)

    @assert size(A,1) == size(A,2) "Matrix is not square."
    q = size(A,2)-1

    # loop over quadrature points
    for l in 1:length(Q)

        # get quadrature point
        x, y, w = Q.x[1][l], Q.x[2][l], Q.w[l]

        # compute bernstein polynomials
        bx = Bx(x)
        by = By(y)

        # loop over functions
        for j in 1:q+1
            for i in 1:q+1
                A[i,j] += bx[i] * by[j] * w
            end
        end
    end
end

function integrate_bernstein_basis(q, element)

    # domain of cut element
    Ix, Iy = element.data

    # quadrature rules
    Qx, Qy = GaussRule(Legendre, q+1, Ix), GaussRule(Legendre, q+1, Iy)
    
    # compute moments of Bernstein functions
    return  bernstein(Ix, q, Qx.x) * Qx.w * (bernstein(Iy, q, Qy.x) * Qy.w)'
end

function get_cut_domain(R::Real, θ::Real, Z::Real)

    # element midpoint position
    c = x_coord_on_circle(R,θ), y_coord_on_circle(R,θ)

    # reference element
    e_ref = Interval(c[1] - 0.5, c[1] + 0.5) ⨱ Interval(c[2] - 0.5, c[2] + 0.5)
    
    # reference direction
    v = x_coord_on_circle(1,θ), y_coord_on_circle(1,θ)

    # coordinates projected on radial curve
    z1 = abs(sum((e_ref[1,1] .- c) .* v))
    z2 = abs(sum((e_ref[2,1] .- c) .* v))
    z3 = abs(sum((e_ref[1,2] .- c) .* v))
    z4 = abs(sum((e_ref[2,2] .- c) .* v))  
    ΔR = max(z1, z2, z3, z4)

    # compute new center
    C = c .+ Z .* ΔR .* v

    return Interval(C[1] - 0.5, C[1] + 0.5) ⨱ Interval(C[2] - 0.5, C[2] + 0.5)
end

function get_bernsteinfuns_at_gausspoints(; order)
    x = get_stencil(Interval(0.0,1.0), order = order+1)
    b = bernstein(Interval(0.0,1.0), order, x)
    return KroneckerProduct(b, b)
end

function generate_training_data(; order, data)

    # arrays to store training data
    xtrain = zeros((order+1)^2, 2*length(data))
    ytrain = zeros((order+1)^2, 2*length(data))

    # compute integrals of Bernstein functions on uncut reference element
    moments_ref = integrate_bernstein_basis(order, Interval(0.0,1.0) ⨱ Interval(0.0,1.0))

    # data-point
    for k in 1:length(data)

        # current data-point
        r, θ, Δr = data[k]

        # get cut-element
        element = get_cut_domain(r, θ, Δr)

        # compute distance function at Gauss-Legendre stencil
        x, y = collect(get_stencil(element, order = order+1))
        distance = evaluate_distance_function(r, x, y)

        # set radius of Cxx DistanceMap
        Main.set_radius!(Main.DistanceFunction, r)

        # compute moments
        moments = integrate_bernstein_basis(order, Main.DistanceFunction, element; n=16)

        # save training data
        xtrain[:,2k-1] = distance[:]
        ytrain[:,2k-1] = moments[:]

        xtrain[:,2k]   = -distance[:]
        ytrain[:,2k]   = moments_ref[:] - moments[:] 
    end

    return xtrain, ytrain
end