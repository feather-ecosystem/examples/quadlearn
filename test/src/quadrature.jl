using UnivariateSplines

export get_stencil, evaluate_distance_function

UnivariateSplines.GaussRule(method, n, I::Interval) = GaussRule(method, n, I.a, I.b)

function get_stencil(I::Interval; order)
    qx = GaussRule(Legendre, order, I)
    return qx.x
end

function get_stencil(element::CartesianProduct{2}; order)
    return get_stencil(element.data[1], order=order) ⨱ get_stencil(element.data[2], order=order)
end

function evaluate_distance_function(r, x, y)
    return sqrt.(x.^2 + y.^2) .- r;
end
