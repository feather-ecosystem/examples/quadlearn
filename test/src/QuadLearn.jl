module QuadLearn

using Cxx, Libdl, LinearAlgebra
using Algoim, CartesianProducts, KroneckerProducts

import AbstractMappings: Interval

export AlgoimMapping, AlgoimQuadRule
export Interval
export CartesianProducts, ⨱

include("setup.jl")
include("area.jl")
include("bernstein.jl")
include("quadrature.jl")
include("trainingdata.jl")

end # module 