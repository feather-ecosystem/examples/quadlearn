using Test, Revise

using Algoim

using QuadLearn, CartesianProducts
using LinearAlgebra, StatsBase

# initialize cpp-julia interface variables
include(joinpath(dirname(pathof(Algoim)), "setup.jl"))

# unit-circle
cxx"""
#include "blitz/array.h"

struct Circle
{
    double radius = 1.0;

    template<typename T>
    T operator() (const blitz::TinyVector<T, 2>& x) const
    {
        return x(0) * x(0) + x(1) * x(1) - radius*radius;
    }

    template<typename T>
    blitz::TinyVector<T, 2> grad(const blitz::TinyVector<T, 2>& x) const
    {
        return blitz::TinyVector<T, 2>(2.0 * x(0), 2.0 * x(1));
    }
};
"""

const DistanceFunction = AlgoimMapping{2}(@cxx Circle())

function set_radius!(map, r::Real)
    F = map.cxx_map
    icxx"$F.radius = $r;" 
end

using Flux, Statistics
using Flux.Data: DataLoader
using Flux: @epochs
using Flux.Losses: mse
using Base: @kwdef
using CUDA

@kwdef mutable struct Args
    order::Int=2
    η::Float64 = 3e-4       ## learning rate
    batchsize::Int = 128    ## batch size
    epochs::Int = 10000     ## number of epochs
    use_cuda::Bool = true   ## use gpu (if cuda available)
end

function getdata(args)
    ENV["DATADEPS_ALWAYS_ACCEPT"] = "true"

    # training data points
    R  = [1.0; 1.5; 2.3; 3.4; 5.0; 7.5; 11.0]     # circle radius
    Θ  = LinRange(0.0, 2*pi, 16)    # angle in radians
    ΔR = LinRange(-0.9, 0.9, 11)    # steps in radial direction

    # compute train and testset
    xtrain, ytrain = generate_training_data(order=args.order, data=CartesianProduct(R, Θ, ΔR))
    xtest, ytest = generate_training_data(order=args.order, data=CartesianProduct(midpoints(R), midpoints(Θ), midpoints(ΔR)))

    ## Create two DataLoader objects (mini-batch iterators)
    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest), batchsize=args.batchsize)

    return train_loader, test_loader
end

# We define the model with the `build_model` function: 
function build_model(; order=4)
    n = (order+1)^2
    return Chain( Dense(n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, n, tanh))
end

# Now, we define the loss function `loss_and_accuracy`. It expects the following arguments:
# * ADataLoader object.
# * The `build_model` function we defined above.
# * A device object (in case we have a GPU available).
function loss(A, data_loader, model, device)
    ls = 0.0
    num = 0
    for (x, y) in data_loader
        x, y = device(x), device(y)
        ŷ = model(x)
        ls += mse(A * ŷ, y; agg = sum) 
        num +=  size(x, 2)
    end
    return ls / num
end

function accuracy(A, data_loader, model, device)
    max_e = -1.0
    min_e = 1.0
    ave_e = 0.0
    num = 0
    for (x, y) in data_loader
        x, y = device(x), device(y)
        error = sqrt.(sum((A * model(x) - y).^2, dims=1))
        t_min, t_max = minimum(error), maximum(error)
        
        min_e = t_min < min_e ? t_min : min_e
        max_e = t_max > max_e ? t_max : max_e
        ave_e += sum(error)

        num += size(x,2)
    end
    return min_e, ave_e / num, max_e
end

#function train(; kws...)

    ## Collect options in a struct for convenience
    # args = Args(; kws...)


args = Args()


CUDA.has_cuda_gpu()

if CUDA.functional() && args.use_cuda
    @info "Training on CUDA GPU"
    CUDA.allowscalar(false)
    device = gpu
else
    @info "Training on CPU"
    device = cpu
end

## Create test and train dataloaders
train_loader, test_loader = getdata(args)

## Construct model
model = build_model(order=args.order) |> device
ps = Flux.params(model) ## model's trainable parameters

# Optimizer
opt = ADAM(args.η)

# mapping of output (weights -> moments)
A = device(get_bernsteinfuns_at_gausspoints(order=args.order))

## Training
@time begin
for epoch in 1:1000 #args.epochs
    for (x, y) in train_loader
        x, y = device(x), device(y) ## transfer data to device
        gs = gradient(() -> mse(A * model(x), y; agg=sum), ps)  ## compute gradient
        Flux.Optimise.update!(opt, ps, gs)      ## update parameters
    end

    ## Report on train and test
    train_loss = loss(A,train_loader, model, device)
    test_loss = loss(A,test_loader, model, device)
    train_min_e, train_ave_e, train_max_e = accuracy(A,train_loader, model, device)
    test_min_e, test_ave_e, test_max_e = accuracy(A,test_loader, model, device)
    println("Epoch=$epoch")
    println("  train loss = $train_loss")
    println("  train accuracy = ($train_min_e, $train_ave_e, $train_max_e)")
    println("  test loss = $test_loss")
    println("  test accuracy = ($test_min_e, $test_ave_e, $test_max_e)")
    
end
end
# end

x, y = train_loader.data

model(x)

z = sum((A * model(x) - y).^2, dims=1)

mse(A * model(x[:,1]), y[:,1]; agg = sum)

reshape(model(x[:,1]), 5,5)