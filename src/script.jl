using Test, Revise

using KroneckerProducts

using Flux, Statistics
using Flux.Data: DataLoader
using Flux: @epochs
using Flux.Losses: mse
using Base: @kwdef
using CUDA

using FileIO, JLD2

@kwdef mutable struct Args
    order::Int=2
    η::Float64 = 3e-4       ## learning rate
    batchsize::Int = 128    ## batch size
    epochs::Int = 10000     ## number of epochs
    use_cuda::Bool = true   ## use gpu (if cuda available)
end

function getdata(args)

    # get training and test data
    traindata = FileIO.load("qlearntrainingdata/traindata_q2_R8_theta16_dR11.jld2","traindata")
    testdata = FileIO.load("qlearntrainingdata/testdata_q2_R8_theta16_dR11.jld2","testdata")

    ## Create two DataLoader objects (mini-batch iterators)
    train_loader = DataLoader((traindata.x, traindata.y), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((testdata.x, testdata.y), batchsize=args.batchsize)

    return train_loader, test_loader, traindata.operator
end

# We define the model with the `build_model` function: 
function build_model(; order)
    n = (order+1)^2
    return Chain( Dense(n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, 2*n, tanh),
                  Dense(2*n, n, tanh))
end

# Now, we define the loss function `loss_and_accuracy`. It expects the following arguments:
# * ADataLoader object.
# * The `build_model` function we defined above.
# * A device object (in case we have a GPU available).
function loss(A, data_loader, model, device)
    ls = 0.0
    num = 0
    for (x, y) in data_loader
        x, y = device(x), device(y)
        ŷ = model(x)
        ls += mse(A * ŷ, y; agg = sum) 
        num +=  size(x, 2)
    end
    return ls / num
end

function accuracy(A, data_loader, model, device)
    max_e = -1.0
    min_e = 1.0
    ave_e = 0.0
    num = 0
    for (x, y) in data_loader
        x, y = device(x), device(y)
        error = sqrt.(sum((A * model(x) - y).^2, dims=1))
        t_min, t_max = minimum(error), maximum(error)
        
        min_e = t_min < min_e ? t_min : min_e
        max_e = t_max > max_e ? t_max : max_e
        ave_e += sum(error)

        num += size(x,2)
    end
    return min_e, ave_e / num, max_e
end

#function train(; kws...)

    ## Collect options in a struct for convenience
    # args = Args(; kws...)


args = Args()


CUDA.has_cuda_gpu()

if CUDA.functional() && args.use_cuda
    @info "Training on CUDA GPU"
    CUDA.allowscalar(false)
    device = gpu
else
    @info "Training on CPU"
    device = cpu
end

## Create test and train dataloaders
train_loader, test_loader, A = getdata(args)

## Construct model
model = build_model(order=args.order) |> device
ps = Flux.params(model) ## model's trainable parameters

# Optimizer
opt = ADAM(args.η)

## Training
@time begin
for epoch in 1:1000 #args.epochs
    for (x, y) in train_loader
        x, y = device(x), device(y) ## transfer data to device
        gs = gradient(() -> mse(A * model(x), y; agg=sum), ps)  ## compute gradient
        Flux.Optimise.update!(opt, ps, gs)      ## update parameters
    end

    ## Report on train and test
    train_loss = loss(A,train_loader, model, device)
    test_loss = loss(A,test_loader, model, device)
    train_min_e, train_ave_e, train_max_e = accuracy(A,train_loader, model, device)
    test_min_e, test_ave_e, test_max_e = accuracy(A,test_loader, model, device)
    println("Epoch=$epoch")
    println("  train loss = $train_loss")
    println("  train accuracy = ($train_min_e, $train_ave_e, $train_max_e)")
    println("  test loss = $test_loss")
    println("  test accuracy = ($test_min_e, $test_ave_e, $test_max_e)")
    
end
end
# end